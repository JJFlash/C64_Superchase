Dim scrAddrCache(25) as WORD @loc_scrAddrCache ' 0 -> 24
loc_scrAddrCache:
DATA AS WORD 1024, 1064, 1104, 1144, 1184, 1224, 1264, 1304, 1344, 1384
DATA AS WORD 1424, 1464, 1504, 1544, 1584, 1624, 1664, 1704, 1744, 1784
DATA AS WORD 1824, 1864, 1904, 1944, 1984

poke 53280, 13 : poke 53281, 13
sys $E544 FAST ' clear screen
poke 646, 0 'black chars
locate 0, 24

dim charset_array(2048) as BYTE @LOC_charset_addr
memcpy @charset_array, $3800, 2048
poke $D018, 31 'final character location: $3800

PRINT "         **************"
PRINT "         * superchase *"
PRINT "         **************"
PRINT "{13}{13}{13}{13}   use the arrow keys to move."
PRINT "{13}{13}   try to gather as many"
PRINT "{13}   treasures ({160}) in the maze"
PRINT "{13}   as possible before the"
PRINT "{13}   enemy catches you!"

PRINT "{13}{13}{13}   press any key to begin";

poke 198, 0 : wait 198, 1 : poke 198, 0

line300:
SC = 0
LV = 1
Dim FL as BYTE : FL = 0
Dim CX as INT : CX = 0
Dim CY as INT : CY = 0
Dim CCX as INT : CCX = 0
Dim CCY as INT : CCY = 0
Dim G as BYTE : G = 0
Dim tryAgain$ as STRING * 1

line320:
sys $E544 FAST ' clear screen
poke 53280, 5 ' green
poke 53281, 7 'yellow

memset $0400, 1000, 104 'reverse space
memset $D800, 1000, 6 'blue

Randomize TI()

For I AS BYTE = 1 to 65
	X = 2 * (CBYTE(9*RND())) + 4
	Y = 2 * (CBYTE(11*RND())) + 4
	TEXTAT Y, X, "{176}{176}{176}{176}{176}"
Next I

For I AS BYTE = 1 to 60
	X = 2 * (CBYTE(7*RND())) + 4
	Y = 2 * (CBYTE(13*RND())) + 4
	CHARAT Y, X    , 112
	CHARAT Y, X + 1, 112
	CHARAT Y, X + 2, 112
	CHARAT Y, X + 3, 112
	CHARAT Y, X + 4, 112
Next I

TEXTAT 4, 4, "{176}{176}{176}{176}{176}"
CHARAT 4, 5, 112
CHARAT 4, 6, 112
CHARAT 4, 7, 112
CHARAT 4, 8, 112

For I AS BYTE = 1 to 80
	X = 2 * (CBYTE(9*RND())) + 4
	Y = 2 * (CBYTE(13*RND())) + 4
	CHARAT Y, X, 96, 4 '  4 = magenta
Next I

TEXTAT 3, 23, "score:", 0 'black

'****************************************************************************

T = 0 ' treasures collected

IF LV = 8 Then LV = 7

A = 4
B = 4
C = 4
D = 4
PG = 112

'****************************************************************************

line670:
CHARAT B, A, 120, 2 ' 2 = Red 

IF T < 8-LV THEN goto line1410

IF FL = 0 THEN goto line740

IF (FL AND 1) = 0 THEN Goto line1140

CCX = CX
CCY = CY
FL = 0

line740:             '"PRIVILEGED" CHECK -------------------------------------------------
GC = peek(scrAddrCache(C + 1) + D)
IF GC  = 120 THEN goto line1760
IF GC  = 104 THEN goto line810
IF GC <> 128 THEN goto line810
CX = 1
CY = 0
GOTO line1340

line810:
GC = peek(scrAddrCache(C) + D + 1)
IF GC  = 120 THEN goto line1760
IF GC  = 104 THEN goto line880
IF GC <> 128 THEN goto line880
CX = 0
CY = 1
GOTO line1340

line880:
GC = peek(scrAddrCache(C - 1) + D)
IF GC  = 120 THEN goto line1760
IF GC  = 104 THEN goto line950
IF GC <> 128 THEN goto line950
CX = -1
CY = 0
GOTO line1340

line950:
GC = peek(scrAddrCache(C) + D - 1)
IF GC = 120 THEN goto line1760
IF GC = 128 THEN goto line1320
IF GC = 112 THEN goto line1320

FL = FL + 1
IF (FL AND 1) = 0 then goto line1040

CX = CCX
CY = CCY
GOTO line1160

line1040:        '-----------CHECK USING SGN - ***NEVER EXECUTES!!***
CX = SGN(A - C)
CY = 0
IF CX <> 0 THEN goto line1080
CY = SGN(D - B)

line1080:
GC = peek(scrAddrCache(C + CX) + D + CY)
IF GC = 104 THEN goto line1110
IF (GC = 96) OR (GC = 112) THEN goto line1340

line1110:
CX = -1
CY = 0
GOTO line1160

line1140:
FL = FL + 1
IF FL > 1 THEN goto line1300

line1160:           '---------------"RELAXED" CHECK
GC = peek(scrAddrCache(C + CX) + D + CY)
IF (GC = 112) OR (GC = 96) OR (GC = 128) THEN goto line1340

CX = 1
CY = 0
GC = peek(scrAddrCache(C + CX) + D)
IF (GC = 112) OR (GC = 96) OR (GC = 128) THEN goto line1340

CX = -1
GC = peek(scrAddrCache(C + CX) + D)
IF (GC = 112) OR (GC = 96) OR (GC = 128) THEN goto line1340

CX = 0
CY = -1
GC = peek(scrAddrCache(C) + D + CY)
IF (GC = 112) OR (GC = 96) OR (GC = 128) THEN goto line1340

CY = 1

line1300:
GC = peek(scrAddrCache(C + CX) + D + CY)
IF GC = 104 THEN goto line1390 ELSE goto line1340

line1320:
CX = 0
CY = -1

line1340:
IF PG = 96 THEN goto line1360
PG = 112 + FL

line1360:
CHARAT D, C, PG, 4 '4 = magenta
C = C + CX
D = D + CY

line1390:
CHARAT D, C, 42, 0 ' 0 = black
PG = GC
'****************************************************************************

line1410:
K1 = peek( $DC00) : K1 = 127 - K1
if K1 = 0 OR K1 > 8 then goto line1410

'Wait for the stick to be released, otherwise the game runs too fast
do
loop until peek ( $DC00) = 127

CHARAT B, A, 128, 1 ' 1 = White
'1: up - 2: down - 4: left - 8: right
on K1 goto line1410, line1690, line1540, line1410, line1590, line1410, line1410, line1410, line1640

line1450:
IF G <> 96 then goto line670
poke 53280, 13 : wait $d011, 128 : wait $d011, 128, 128 : wait $d011, 128 : wait $d011, 128, 128 : poke 53280, 5 ' light green, green
SC = SC + 1
textat 10, 23, str$(SC), 0
T = T + 1 ' treasures collected
IF T < 45 then goto line670 else LV = LV + 1 : Goto line320

line1540:
G = peek(scrAddrCache(A + 1) + B)
IF G = 104 then goto line1740
IF G = 42  then goto line1760
A = A + 1
Goto line1450

line1590:
G = peek(scrAddrCache(A) + B - 1)
IF G = 104 then goto line1740
IF G = 42  then goto line1760
B = B - 1
Goto line1450

line1640:
G = peek(scrAddrCache(A) + B + 1)
IF G = 104 then goto line1740
IF G = 42  then goto line1760
B = B + 1
Goto line1450

line1690:
G = peek(scrAddrCache(A - 1) + B)
IF G = 104 then goto line1740
IF G = 42  then goto line1760
A = A - 1
Goto line1450

line1740:
poke 53280, 2 : wait $d011, 128 : wait $d011, 128, 128 : poke 53280, 5 ' red, green
goto line670
'****************************************************************************

line1760:
FOR I = 1 to 3
	poke 53280, 2
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	poke 53280, 4
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
next I
FOR I = 1 to 3
	poke 53280, 1 : poke 53281, 1 'white
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	
	poke 53280, 10 : poke 53281, 10 'light red
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	
	poke 53280, 3 : poke 53281, 3 'cyan
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
	wait $d011, 128 : wait $d011, 128, 128
next I

locate 0, 24
PRINT "got caught!!!"
PRINT "try again? (y/n)";

line1840:
poke 198, 0 : wait 198,1
get tryAgain$
if tryAgain$ = "y" then goto line300
if tryAgain$ <> "n" then goto line1840

poke 53280, 14 : poke 53281, 6
memset $d800, 1000, 14
poke 646, 14
poke $D018, 21 'original character location
memcpy $E3A2, $73, $17 'CHRGET bug workaround

END

LOC_charset_addr:
incbin "charset.chr"
