# Superchase - Commodore 64 version

A quest to convert an old BASIC game for the TI-99 4/A computer to [XC=Basic 3](https://xc-basic.net/) (the game itself is a rewriting of a game listing for Atari 8-bit and VIC-20).

The game is "Superchase", I found it in Compute!'s "First Book of TI Games" on [archive.org](https://archive.org/details/computesfirstbookofti994agames/) .

This ended up becoming a **remix** of three different versions of the same game:

- Original VIC-20 version by Anthony Godshall - October 1982
- Atari version by someone @ Compute! Gazette  (took mainly the graphics from it)
- TI-99 4/A version by Cheryl Regena  (the monster-logic was totally broken, I merely got inspired by it)

**FINALLY** completed this on November 1st, 2022 - forty years later the first publication in Compute Gazette! I didn't manage to keep everything into the first VIC bank (first 16K of C64 RAM), but... I got pretty close  :)